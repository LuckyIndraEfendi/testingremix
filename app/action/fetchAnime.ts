import { json } from "@remix-run/node";

export const loader = async () => {
  const data = await fetch("https://api.jikan.moe/v4/anime?q=naruto&sfw");
  const response = await data.json();
  return json(response);
};
