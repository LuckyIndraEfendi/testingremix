import { useLoaderData, useRevalidator } from "@remix-run/react";
import { json } from "@remix-run/node";

type AnimeProps = {
  title: string;
  images: {
    webp: {
      image_url: string;
    };
  };
};

export const loader = async () => {
  const data = await fetch("https://api.jikan.moe/v4/anime?q=naruto&sfw");
  const response = await data.json();
  return json(response);
};

export default function About() {
  const { data } = useLoaderData<typeof loader>();
  console.log(data);
  const revalidator = useRevalidator();
  const handleRefetch = () => {
    revalidator.revalidate();
  };
  return (
    <>
      <button onClick={handleRefetch}>Refetch</button>
      {data?.map((anime: AnimeProps, i: number) => (
        <div className="" key={i}>
          <img src={anime.images.webp.image_url} alt="" />
          <h1>{anime.title}</h1>
        </div>
      ))}
    </>
  );
}
