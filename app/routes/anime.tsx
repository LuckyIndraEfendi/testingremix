import { useLoaderData } from "@remix-run/react";
type AnimeProps = {
  title: string;
};
export async function clientLoader() {
  const data = await fetch("https://api.jikan.moe/v4/anime?q=naruto&sfw");
  const response = await data.json();
  return response;
}
export function HydrateFallback() {
  return <p>Loading Anime...</p>;
}

export default function Anime() {
  const data = useLoaderData<typeof clientLoader>();

  return (
    <>
      {data?.data?.map((anime: AnimeProps, i: number) => (
        <h1 key={i}>{anime.title}</h1>
      ))}
    </>
  );
}
